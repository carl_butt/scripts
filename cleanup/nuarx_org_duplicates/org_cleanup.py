#!/usr/bin/env python

import pycurl, certifi, json
from io import BytesIO


with open('dupes.txt') as file:
    external_ids=file.read().splitlines()


for i, external_id in enumerate(external_ids):
    buffer = BytesIO()
    c = pycurl.Curl()
    c.setopt(c.URL, 'http://10.128.0.12:9200/org/_search?q=externalId:' + external_id)
    c.setopt(c.WRITEDATA, buffer)
    c.setopt(c.CAINFO, certifi.where())
    c.perform()
    c.close()

    body = buffer.getvalue()
    j = json.loads(body)
    hits = j['hits']['hits']
    if len(hits) != 2:
        print ("Error on external_id " + external_id)
        exit(1)

    bad_ids = [hit['_source']['id'] for hit in hits if '2022-03-16' in
                hit['_source']['createdInstant']]

    if len(bad_ids) != 1:
        print("Error on bad ids for external_id " + external_id)
        exit(1)

    for bad_id in bad_ids:
        print("Deleting id: " + bad_id + ", " + str(i))
        d = pycurl.Curl()
        d.setopt(d.URL, 'http://10.128.0.12:9200/org/_doc/' + bad_id)
        d.setopt(d.CUSTOMREQUEST, 'DELETE')
        d.setopt(d.CAINFO, certifi.where())
        d.perform()
        d.close()
