#!/usr/bin/env python

import json, time
#from io import BytesIO
from elasticsearch import Elasticsearch
from google.cloud import storage

ENV_CONFIG = {"PROD": {}, "DEV": {}}

ENV_CONFIG["PROD"]["es_url"] = "http://10.128.0.12:9200"
ENV_CONFIG["DEV"]["es_url"] = "http://10.128.0.125:9200"

ENV_CONFIG["PROD"]["index"] = "file-locker"
ENV_CONFIG["DEV"]["index"] = "file-locker-archive-test"

ENV_CONFIG["PROD"]["gcs_bucket"] = "prod-file-locker"
ENV_CONFIG["DEV"]["gcs_bucket"] = "dev-1-file-locker"

ENV_CONFIG["PROD"]["assessment_ids"] = ['576808059ad57e5cec35a978edd8940f',
                                    'fc14d2f06bce78a632554f26f05fa133']
ENV_CONFIG["DEV"]["assessment_ids"] = ['3hR8Zeh14LRSD2sZTrH1YA']

ENV_CONFIG["PROD"]["old_org"] = "YQzD_nnx3xT2wVkpf71rvw"
ENV_CONFIG["PROD"]["new_org"] = "0iZgkB34sf-XiKvu4jkEJg"
ENV_CONFIG["DEV"]["old_org"] = "k6qEX60K_Rr4TlscO2z4ow"
ENV_CONFIG["DEV"]["new_org"] = "0iZgkB34sf-XiKvu4jkEJg"


def make_update_queries(config):
    return [{
        'script': {
            'lang': 'painless',
            'source':
            f"ctx._source.lastUpdated={int(time.time())};" +
            "if (ctx._source.storageKey.contains('%s'))" % config["old_org"] +
              ("{ctx._source.storageKey=ctx._source.storageKey.replace('%s','%s')}" %
               (config["old_org"], config["new_org"])) +
            "if (ctx._source.tags!=null && !ctx._source.tags.contains('%s'))" % config['tag'] +
              ("{ctx._source.tags.add('%s')}" % config['tag']) +
            "def output_paths = new ArrayList();" +
            "ctx._source.orgPaths.stream().forEach(path->{" +
              f"output_paths.add(path.replace('{config['old_org']}', '{config['new_org']}'))"
            "});" +
            "ctx._source.orgPaths=output_paths';" +
            f"ctx._source.orgId='{config['new_org']}'"
        },
        'query': {
            "term": {
                "tags": "/vc/assessment-id/=" + assessment_id
            }
        }
    } for assessment_id in config['assessment_ids']]

def main(where):
    config = ENV_CONFIG[where]
    config['tag'] = "/pvt/org-merge-script-" + config["new_org"] + "/=" + config["old_org"]
    es_client = Elasticsearch(config["es_url"])
    storage_client = storage.Client()

#    Perform google move
    bucket = config['gcs_bucket']
    print(config)
    for assessment_id in config['assessment_ids']:
        get_query = {"query": {"term": {
            "tags": "/vc/assessment-id/=" + assessment_id}}}

        results = es_client.search(index=config['index'], body=get_query, size=200)
        storage_keys = [doc['_source']['storageKey'] for doc in results['hits']['hits']]
        print(sorted([key.split('/')[1] for key in storage_keys]))
        for storage_key in storage_keys:
            bucket = storage_client.get_bucket(bucket)
            old_key = storage_key.replace(config['new_org'], config['old_org'])
            print(f"copying {old_key} to {storage_key}")
            blob = bucket.blob(old_key)
            try:
                bucket.copy_blob(blob, bucket, new_name=storage_key)
#            bucket.delete_blob(storage_key)
            except Exception:
                print("old file not found")
# #    Perform elasticsearch update
#     for update in make_update_queries(config):
#         print(update)
#         es_client.update_by_query(index=config['index'], body=update)

main("PROD")
