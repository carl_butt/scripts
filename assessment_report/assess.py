#!/usr/bin/env python

import pycurl, certifi, json, csv
#from io import BytesIO
from elasticsearch import Elasticsearch

ORG_DICT = {}
QUERY_SIZE = 2000
QUERY = {
  "query": {
    "bool" : {
      "must": [{
        "term": {
          "assessment.tags": "/pvt/assessment-type-mst"
        }
      },
      {
        "term" : {
          "assessment.deleted": 'false'
        }
      }]
    }
  }
}

def main():
    es_client = Elasticsearch("http://10.128.0.56:9200")
    org_es_client = Elasticsearch("http://10.128.0.12:9200")
    results = es_client.search(index="assessment", body=QUERY, size=QUERY_SIZE)

    with open("assessments.csv", "w") as csv_file:
        w = csv.writer(csv_file)
        csv_file.write('Org, Assessment Name, Assessment Id, Assessment Status, Users\n')
        print_results(results, org_es_client, w)

def org_name(org_id, es_client):
    if org_id not in ORG_DICT:
        results = es_client.search(index="org",
                                body={"query": {"term": {"id": org_id}}})
        try:
            ORG_DICT[org_id] = results['hits']['hits'][0]['_source']['name']
        except IndexError:
            ORG_DICT[org_id] = org_id # If we don't have a name for the id, use the id
            print("Error on org " + org_id)
    return ORG_DICT[org_id]

def print_results(results, es_client, csv_writer):
    for doc in results['hits']['hits']:
        result = doc['_source']

        csv_writer.writerow([
            org_name(result["assessment"]["org"], es_client),
            result["assessment"]["name"],
            result["assessment"]["id"],
            result["assessment"]["status"],
            [client['username'] for client in result["assessment"]["clientUsers"]]
        ])

main()
