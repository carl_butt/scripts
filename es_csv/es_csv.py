#!/usr/bin/env python

import pycurl, certifi, json
#from io import BytesIO
from elasticsearch import Elasticsearch

QUERY_SIZE = 1000

def format_addresses(result):
    if result.get('addresses', {}) == {}:
        return ''

    return ";".join([address["addressLine1"] + " " + address["city"] + " " +
                     address["province"] + " " + address["postalCode"]
                     for address in result['addresses']])

def print_results(results):
    for doc in results['hits']['hits']:
        result = doc['_source']
        if result['externalId'] is None:
            result['externalId'] = ""
#        if result.get('addresses', {}) == {}:
#            result['addresses'] = {}

        print(result["name"] + ", "+ result["id"] + ", " + result["externalId"]
              + ", " + format_addresses(result)
              + ", " +  nuarx_status(result["externalId"]))


def nuarx_status(external_id):
    if external_id.startswith("nuarx"):
        return "NUARX"
    if external_id != '':
        return "TRUSTWAVE"
    return "INTERNAL"

query = {"_source":["name","addresses","id","externalId"], "size": QUERY_SIZE,
         "sort": [{"id":"asc"}]}

print('Name, Id, Address, External Id, Source')

es_client = Elasticsearch("http://10.128.0.12:9200")
# First query and print
results = es_client.search(index="org", body=query)
print_results(results)
last_seen = results['hits']['hits'][-1]['_source']['id']
query['search_after'] = [last_seen]

i = len(results['hits']['hits'])
# Query and print with search_after
while len(results['hits']['hits']) == QUERY_SIZE:
    results = es_client.search(index="org", body=query)
    i += len(results['hits']['hits'])

    print_results(results)
    last_seen = results['hits']['hits'][-1]['_source']['id']
    query['search_after'] = [last_seen]
