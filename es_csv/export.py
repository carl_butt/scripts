#!/usr/bin/env python

import pycurl, certifi, json, csv
#from io import BytesIO
from elasticsearch import Elasticsearch

QUERY_SIZE = 1000

def print_results(results, w):
    for doc in results['hits']['hits']:
        result = doc['_source']
        w.writerow(result)

query = {
    "query": {
        "bool": {
            "must": [{
                "range": {
                    "createdInstant": {
                        "gte": "now-1d/d",
                        "lte": "now"
                    }
                }
            },
            {
                "term": {
                    "category": "IDS Alert"
                }
            },
            {
                "term": {
                    "source.name": "utm_security_event"
                }
            },
            {
                "prefix": {
                    "orgPaths": "/org/niWTKJQpujKJWKnA11b81w/MmYVVoh8Og_btignvgymfQ"
                }
            }]
        }
    },
    "size": QUERY_SIZE,
    "sort": [{"id":"asc"}]
 }


def main():
    with open("lc_export.csv", "w") as csv_file:
        es_client = Elasticsearch("http://10.128.0.12:9200")
        # First query and print
        results = es_client.search(index="event-2022.07.2*", body=query, request_timeout=30)
        w = csv.DictWriter(csv_file, fieldnames=results['hits']['hits'][0]['_source'].keys())
        w.writeheader()
        print_results(results, w)
        last_seen = results['hits']['hits'][-1]['_source']['id']
        query['search_after'] = [last_seen]

        i = len(results['hits']['hits'])
        # Query and print with search_after
        while len(results['hits']['hits']) == QUERY_SIZE:
            results = es_client.search(index="event-2022.07.2*", body=query, request_timeout=30)
            i += len(results['hits']['hits'])

            print_results(results, w)
            last_seen = results['hits']['hits'][-1]['_source']['id']
            query['search_after'] = [last_seen]

main()
